# -*- coding: utf-8 -*-
import os
import caffe
import re
import numpy as np
import time
from sklearn.metrics import roc_curve, auc
import sklearn.metrics.pairwise as pw
import matplotlib.pyplot as plt


def get_time_str():
    return time.strftime("%Y-%m-%d, %H:%M:%S ", time.localtime((time.time()) ))


def print_info(msg):
    print get_time_str(), msg


def cal_accuracy_roc(y_score, y_true):
    fpr, tpr, thresholds = roc_curve(y_true, y_score)
    # print fpr, len(fpr)
    roc_auc = auc(y_true, y_score)

    p_num = 3000
    n_num = 3000
    t_p_num = p_num * tpr
    t_n_num = n_num * (1 - fpr)
    accuracy = (t_p_num + t_n_num)/(p_num + n_num)
    # print accuracy
    # accuracy = 
    top_accuracy = np.max(accuracy)
    top_index = np.argmax(accuracy)

    # print top_accuracy, thresholds[top_index]

    # Plot ROC curve
    plt.subplot(2, 1, 1)
    plt.plot(fpr, tpr, label='ROC curve (area = %0.3f)' % roc_auc)
    # plt.plot([0, 0.5], [0, 1.0], 'k--')  # random predictions curve
    # plt.plot([0, 0.2], [0, 1.0])  # random predictions curve
    plt.plot([0, 1.0], [0, 1.0], 'k--')  # random predictions curve
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.xlabel('False Positive Rate or (1 - Specifity)')
    plt.ylabel('True Positive Rate or (Sensitivity)')
    plt.title('Receiver Operating Characteristic')
    plt.legend(loc="lower right")

    plt.subplot(2, 1, 2)
    plt.plot(thresholds, accuracy, label='accuracy = %0.3f\nthreshold = %.4f'
             % (top_accuracy, thresholds[top_index]))
    plt.legend(loc="upper left")

    plt.savefig('roc.png')

    return (top_accuracy, thresholds[top_index])


def sigmoid(inx):
    return 1.0 / (1 + np.exp(-inx))


def cosine_distance(x1, x2):
    return np.dot(x1, x2)/(np.linalg.norm(x1) * np.linalg.norm(x2))


def L2_norm(x1, x2):
    L2 = np.linalg.norm(x1 - x2)
    return 1/L2


def prepare_lfw_data(image_path, pairs_list):
    fid = open(pairs_list)
    pairs = fid.readlines()
    fid.close()

    labels = []
    images = []

    for pair in pairs:
        pair = pair.split()
        labels.append(int(pair[-1]))
        pair = pair[:- 1]
        for line in pair:
            if line[-1] == '\n':
                line = line[:-1]
            file_path = os.path.join(image_path, line)
            # print file_path
            im = caffe.io.load_image(file_path)
            images.append(im)

    # pickle.dump(images, open('lfw_pairs.bin', 'w'))
    # pickle.dump(labels, open('lfw_pairs_labels.bin', 'w'))

    return images, labels


def get_scores(net, transformer, images, labels, data_layer, feature_layer):
    # images = pickle.load(open(images_file, 'r'))
    # labels = pickle.load(open(labels_file, 'r'))

    scores = []
    # scores_str = []
    # print images.shape(), labels.shape()
    for i in range(len(labels)):
        # print i
        net.blobs[data_layer].data[...] = transformer.preprocess(data_layer, images[i * 2])
        net.forward()
        fe_0 = net.blobs[feature_layer].data.flatten()
        # fe_0.reshape(1, -1)
        net.blobs[data_layer].data[...] = transformer.preprocess(data_layer, images[i * 2 + 1])
        net.forward()
        fe_1 = net.blobs[feature_layer].data.flatten()
        # fe_0.reshape(1, -1)
        # print fe_0
        # print fe_1
        c_dis = cosine_distance(fe_0, fe_1)
        # c_dis = list(pw.cosine_similarity(fe_0, fe_1))
        # print c_dis
        scores.append(c_dis)
        # scores_str.append(str(c_dis) + ' ')
        
    # fid = open('lfw_score.txt', 'w')
    # fid.writelines(scores_str)
    # fid.close()

    return scores


def test_pairs(net, transformer, image_path, pairs_list, score_list, data_layer, feature_layer):
    fid = open(pairs_list)
    pairs = fid.readlines()
    fid.close()
    distance = []

    for line in pairs:
        pair = re.split(' ', line)
        # print pair[0], pair[1]
        fe = []
        for line in pair:
            if line[-1] == '\n':
                line = line[:-1]
            file_path = os.path.join(image_path, line)
            im = caffe.io.load_image(file_path)
            # plt.imshow(im)
            # plt.show()
            net.blobs[data_layer].data[...] = transformer.preprocess(data_layer, im)
            net.forward()
            fe.append(net.blobs[feature_layer].data.flatten().reshape(1, -1))

        # print fe[0].shape
        # print 'fe_0: \n', fe[0]
        # print 'fe_1: \n', fe[1]
        # c_dis = cosine_distance(fe[0], fe[1])
        # fe[0].reshape(-1, 1)
        c_dis = pw.cosine_similarity(fe[0], fe[1])
        # c_dis = L2_norm(fe[0], fe[1])
        distance.append(str(c_dis) + ' ')

    fid = open(score_list, 'w')
    fid.writelines(distance)
    fid.close()

    # plt.plot(distance)
    # plt.show()
    return distance


def init_caffe_test(data_layer, solver, model):
    net = caffe.Net(solver, model, caffe.TEST)
    # image preprocess
    # set data shape(1, 3, 64, 64)
    transformer = caffe.io.Transformer({data_layer: net.blobs[data_layer].data.shape})
    # set dim order （64，64，3）-> (3, 64, 64)
    transformer.set_transpose(data_layer, (2, 0, 1))
    # substract mean
    # mean_value = [(‘B’, 102.9801), (‘G’, 115.9465), (‘R’, 122.7717)]
    mean_value = np.array([102.9801, 115.9465, 122.7717])
    transformer.set_mean(data_layer, mean_value)
    # scale
    transformer.set_raw_scale(data_layer, 255)
    transformer.set_input_scale(data_layer, 0.00390625)
    # swap order rgb->bgr
    transformer.set_channel_swap(data_layer, (2, 1, 0))

    net.blobs[data_layer].reshape(1, 3, 64, 64)

    return net, transformer


if __name__ == '__main__':
    print_info('\n')
    solver = 'model_64/deepID_deploy.prototxt'
    # model = 'model_64/snapshot_iter_540000.caffemodel'
    model = 'finetune_2/deepid.caffemodel'
    # solver = 'example/deploy.prototxt'
    # model = 'example/snapshot_iter_750000.caffemodel'
    caffe.set_mode_gpu()
    # net = caffe.Net(solver, model, caffe.TEST)

    print_info('caffe.Net done\n')

    net, transformer = init_caffe_test('data_1', solver, model)

    print_info('init_data_transformer done\n')

    images, labels = prepare_lfw_data('/data/Datasets/LFW/lfw-align-2', 'lfw_test_pair.txt')

    print_info('prepare_lfw_data done\n')

    scores = get_scores(net, transformer, images, labels, 'data_1', 'deepid_1')

    # print len(scores)

    cal_accuracy_roc(scores, labels)

    # # image preprocess
    # # set data shape(1, 3, 64, 64)
    # transformer = caffe.io.Transformer({'data_1': net.blobs['data_1'].data.shape})
    # # set dim order （64，64，3）-> (3, 64, 64)
    # transformer.set_transpose('data_1', (2, 0, 1))
    # # substract mean
    # # mean_value = [(‘B’, 102.9801), (‘G’, 115.9465), (‘R’, 122.7717)]
    # mean_value = np.array([102.9801, 115.9465, 122.7717])
    # transformer.set_mean('data_1', mean_value)
    # # scale
    # transformer.set_raw_scale('data_1', 255)
    # transformer.set_input_scale('data_1', 0.00390625)
    # # swap order rgb->bgr
    # transformer.set_channel_swap('data_1', (2, 1, 0))

    # net.blobs['data_1'].reshape(1, 3, 64, 64)

    # image_path = '/data/Datasets/LFW/lfw-align-2'
    # pair_list = 'lfw_pos_jpg.txt'
    # score_list = 'positive_score_2.txt'
    # p_score = test_pairs(net, transformer, image_path, pair_list, score_list)
    # pair_list = 'lfw_neg_jpg.txt'
    # score_list = 'negitive_score_2.txt'
    # n_score = test_pairs(net, transformer, image_path, pair_list, score_list)

    # # cal_roc.cal_roc(p_score, n_score, 'positive_score_2.txt', 'negitive_score_2.txt')
    # cal_roc.cal_roc('positive_score_2.txt', 'negitive_score_2.txt')
    # print_info('end deepid_test')
