import numpy as np
import matplotlib.pyplot as plt

def plot_curve(train_loss, test_loss, test_acc, lfw_acc, display, test_interval):
    # draw train loss test loss and accuracy curve
    # print '\nplot the train loss and test accuracy\n'
    # plt.close('all')
    fig = plt.figure(figsize=(15, 5))
    # _, ax1 = plt.subplots(121)
    ax1 = fig.add_subplot(121)
    ax2 = ax1.twinx()
    # train loss -> green
    ax1.plot(display * np.arange(len(train_loss)), train_loss, 'g')
    # test loss -> yellow
    ax1.plot(test_interval * np.arange(len(test_loss)), test_loss, 'y')
    # test accuracy -> red
    ax2.plot(test_interval * np.arange(len(test_acc)), test_acc, 'r')
    ax1.set_xlabel('iteration')
    ax1.set_ylabel('loss')
    ax2.set_ylabel('accuracy')
    # plt.savefig('train.png')
    # plt.show()

    # plt.figure()
    ax3 = fig.add_subplot(122)
    ax3.plot(test_interval * np.arange(len(lfw_acc)), lfw_acc)
    ax3.set_xlabel('iteration')
    ax3.set_ylabel('accuracy')
    plt.savefig('train.png')
    # plt.show()