import numpy as np
import matplotlib.pyplot as plt

x1 = np.random.rand(10, 1)
x1 = x1.tolist()
print x1
x2 = 2*x1
x3 = 3*x1
x4 = x1
print x2, x3, x4
plt.close('all')
fig = plt.figure(figsize = (15, 5))
ax1 = fig.add_subplot(121)
# _, ax1 = plt.subplots()
ax2 = ax1.twinx()
# train loss -> green
ax1.plot(x1, 'g')
# test loss -> yellow
ax1.plot(x2, 'y')
# test accuracy -> red
ax2.plot(x3, 'r')
ax1.set_xlabel('iteration')
ax1.set_ylabel('loss')
ax2.set_ylabel('accuracy')
# plt.savefig('train.png')
# plt.show()

# plt.figure()
ax3 = fig.add_subplot(122)
# plt.subplots(122)
ax3.plot(x4)
fig.savefig('test.png')
plt.show()