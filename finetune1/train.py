# -*- coding: utf-8 -*-
import caffe
from face_verification_test import *
from plot_curve import *
import google.protobuf as pb2

caffe.set_mode_gpu()
caffe.set_device(0)

solver_prototxt = 'solver.prototxt'
solver = caffe.SGDSolver(solver_prototxt)
# solver.net.copy_from('/home/ronghuaiyang/Code/DeepID/finetune_5/snapshot_iter_405450.caffemodel')

solver_param = caffe.proto.caffe_pb2.SolverParameter()
with open(solver_prototxt, 'rt') as fd:
    pb2.text_format.Merge(fd.read(), solver_param)

niter = solver_param.max_iter
display = solver_param.display
test_iter = solver_param.test_iter[0]
test_interval = solver_param.test_interval
snapshot = solver_param.snapshot

print niter, display, test_iter, test_interval, snapshot

train_loss = []
test_loss = []
test_acc = []
lfw_acc = []

_train_loss = 0
_test_loss = 0
_accuracy = 0

data_layer = 'data'
loss_layer = 'softmax_loss'
acc_layer = 'accuracy'
feature_layer = 'feature1'

deploy_solver = 'deploy.prototxt'

# net, transformer = init_caffe_test(data_layer, deploy_solver, pre_trained_model)
images, labels = prepare_lfw_data('/data/Datasets/LFW/lfw-align-2',
                                  '/data/Datasets/LFW/lfw_test_pair.txt')
print 'prepare lfw data done'
current_model = 'face_verification.caffemodel'

# while(0):
for it in range(niter):
    solver.step(1)
    _train_loss += solver.net.blobs[loss_layer].data
    if it % display == 0 and it != 0:
        # cal the mean train loss of num = display iters
        train_loss.append(_train_loss / display)
        _train_loss = 0

    if it % test_interval == 0 and it != 0:
        for test_it in range(test_iter):
            # test one iter
            solver.test_nets[0].forward()
            # cal mean test loss
            _test_loss += solver.test_nets[0].blobs[loss_layer].data
            # cal test accuracy
            _accuracy += solver.test_nets[0].blobs[acc_layer].data
        # cal mean test loss
        test_loss.append(_test_loss / test_iter)
        # cal mean test accuracy
        test_acc.append(_accuracy / test_iter)
        _test_loss = 0
        _accuracy = 0

        # feed lfw data
        print_info('start lfw test')
        solver.net.save(current_model)
        if it == test_interval:
            net, transformer = init_caffe_test(data_layer, deploy_solver, current_model)
        net.copy_from(current_model)
        score = get_scores(net, transformer, images, labels, data_layer, feature_layer)
        acc, th = cal_accuracy(score, labels)
        lfw_acc.append(acc)
        print '\nlfw face varification accuracy: ', acc, 'threshold: ', th, '\n'
        print_info('end lfw test')

        plot_curve(train_loss, test_loss, test_acc, lfw_acc, display, test_interval)

    if it % snapshot == 0 and it != 0:
        print_info('start save snapshot')
        solver.snapshot()
        print_info('end save snapshot')
